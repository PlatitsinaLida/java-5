//Напишите класс UpTriangleMatrix (верхнетреугольная матрица).
// Размерность матрицы также задается при ее создании и далее не меняется.

public class UpTriangleMatrix extends Matrix {
    public UpTriangleMatrix(int N)
    {
        super(N);
    }

    @Override
    public void changeElemInd(double elem, int indexI, int indexJ) {
        if (elem!=0 && indexJ>indexI)
        {
            throw new IllegalArgumentException("Not true element or indexes");
        }
        super.changeElemInd(elem, indexI, indexJ);
    }
}
