//Напишите класс Matrix (квадратная матрица произвольного вида).
// Размерность матрицы N задается при создании объекта и в дальнейшем не меняется.
// Матрицу хранить в виде одномерного массива длины NxN. В классе сделайте конструктор по размерности,
// добавьте методы equals и hashCode.

// Добавить в класс Matrix поле для хранения вычисленного значения определителя
// (кэш для значения определителя) и флаг, который означает, что это значение действительно на данный момент.
// Методы, изменяющие матрицу, должны сбрасывать этот флаг.
// При установленном флаге метод вычисления определителя должен просто возвращать сохраненное значение без пересчета.
import java.util.Arrays;
import java.util.Objects;

public class Matrix implements IMatrix{
    private double[] arr;
    private int N;
    private double determinant;
    private boolean isActive=true;

    /* Филиппов А.В. 10.06.2020 Комментарий не удалять.
     Не работает! Вы забыли установить isActive
    */
    //Исправлено
    public Matrix(int N)
    {
        this.arr = new double[N*N];
        this.N = N;
    }

    public int getN() {
        return N;
    }

    public double[] getArr()
    {
        return arr;
    }


    public Matrix matrixRandom(Matrix matrix)
    {
        double a= 0.0;
        double b=10.0;
        for(int i = 0; i<matrix.getN()*matrix.getN(); i++)
        {
            matrix.getArr()[i]=a+(Math.random()*b);
        }
        isActive =false;
        return matrix;
    }

    public double getElemInd(int indexI, int indexJ) {
        if(indexI<0 || indexJ<0 || indexI>=N || indexJ>=N){
            throw new IllegalArgumentException("Index Error");
        }
       return arr[indexI*N+indexJ];
    }


    public void changeElemInd(double elem, int indexI, int indexJ) {
        isActive = true; if(indexI<0 || indexJ<0 || indexI>=N || indexJ>=N){
            throw new IllegalArgumentException("Index Error");
        }
        arr[indexI*N+indexJ]=elem;
        isActive=false;

        /* Филиппов А.В. 10.06.2020 Комментарий не удалять.
         Какая-то строчка isActive=true; лишняя
        */
    }

    public double MatrixDeterminant() {
        double det = determinant;
        if (N == 1) {
            return getElemInd(0, 0);
        }
            if (!isActive) {

                det = 1;
                int h = 0;
                double buf2;
                double[] buf = new double[N * N];
                for (int i = 0; i < N * N; i++) {
                    buf[i] = arr[i];
                }

        /* Филиппов А.В. 10.06.2020 Комментарий не удалять.
         Вы когда-нибудь будете очень сильно жалеть о том, что сравниваете даблы на равенство.
         Например, когда будете делить на 1e-158 это же не ноль.
        */
                for (int i = 1; i < N; i++) {
                    if (Double.compare(buf[h], 0.0) == 0) {
                        for (int k = 1; k < (N - i + 1); k++) {
                            if (Double.compare(buf[k * N + h], 0.0) != 0) {
                                for (int t = 0; t < (N - i + 1); t++) {
                                    buf[h + t] = buf[h + t] + buf[k * N + h + t];
                                }
                                break;
                            }
                        }
                    }
                    if (Double.compare(buf[h], 0.0) != 0) {
                        for (int q = i; q < N; q++) {
                            buf2 = buf[h + (q - i + 1) * N];
                            for (int j = 0; j < N; j++) {
                                buf[j + N * q] = buf[j + N * q] - (buf[j + (i - 1) * N] / buf[h] * buf2);
                            }
                        }
                    }
                    det *= buf[h];
                    h += (N + 1);
                }
                det *= buf[h];
            }
                this.determinant = det;
                isActive = true;
                return determinant;

    }

}
