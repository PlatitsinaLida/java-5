public interface IMatrix {
    double getElemInd(int indexI, int indexJ);
    void changeElemInd(double elem, int indexI, int indexJ);
    double MatrixDeterminant();
}
// Реализовать интерфейс IMatrix (квадратная матрица вещественных чисел), содержащий методы:
//- получить элемент с заданными индексами,
//- изменить элемент с заданными индексами,
//- вычислить определитель матрицы (лучше методом Гаусса).