// Напишите производный класс DiagMatrix (диагональная матрица).
// Размерность матрицы определяется при ее создании и в дальнейшем не меняется.
// Создайте конструктор по размерности матрицы и конструктор по набору элементов на диагонали.
// Метод изменения элемента при попытке записать ненулевое значение вне диагонали должен выбрасывать исключение.

public class DiagMatrix extends Matrix {

    public DiagMatrix(int N) {
        super(N);
    }

    public DiagMatrix(double[] matrix) {
        super(matrix.length);
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (i == j) {
                    super.changeElemInd(matrix[i], i, j);
                }
            }
        }
    }

    /* Филиппов А.В. 10.06.2020 Комментарий не удалять.
     throws IllegalArgumentException - не пишется в заголовке функции это runtime exception
    */
    @Override
    public void changeElemInd(double elem, int indexI, int indexJ){
        if (elem == 0) {
            super.changeElemInd(elem, indexI, indexJ);
        } else if (indexI == indexJ) {
            super.changeElemInd(elem, indexI, indexJ);
        } else {
            throw new IllegalArgumentException("Not true element or indexes");
        }
    }
}
