import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MatrixTest {
    @Test
    public void testMatrixDeterminant() {
        Matrix m = new Matrix(1);
        m.changeElemInd(1, 0, 0);

        Matrix matrix = new Matrix(2);
        matrix.changeElemInd(0, 0, 0);
        matrix.changeElemInd(0, 0, 1);
        matrix.changeElemInd(1, 1, 0);
        matrix.changeElemInd(1, 1, 1);

        Matrix matrix1 = new Matrix(3);
        matrix1.changeElemInd(1, 0, 0);
        matrix1.changeElemInd(0, 0, 1);
        matrix1.changeElemInd(0, 0, 2);
        matrix1.changeElemInd(0, 1, 0);
        matrix1.changeElemInd(1, 1, 1);
        matrix1.changeElemInd(0, 1, 2);
        matrix1.changeElemInd(0, 2, 0);
        matrix1.changeElemInd(0, 2, 1);
        matrix1.changeElemInd(1, 2, 2);

        assertEquals(1, matrix1.MatrixDeterminant(), 1e-9);
        assertEquals(1.0, m.MatrixDeterminant(), 1e-9);
        assertEquals(0.0, matrix.MatrixDeterminant(), 1e-9);
    }

    @Test
    public void testMatrixDeterminant2() {
        Matrix matrix1 = new Matrix(3);
        matrix1.changeElemInd(1, 0, 0);
        matrix1.changeElemInd(1, 0, 1);
        matrix1.changeElemInd(1, 0, 2);
        matrix1.changeElemInd(1, 1, 0);
        matrix1.changeElemInd(1, 1, 1);
        matrix1.changeElemInd(2, 1, 2);
        matrix1.changeElemInd(1, 2, 0);
        matrix1.changeElemInd(2, 2, 1);
        matrix1.changeElemInd(1, 2, 2);

        assertEquals(-1, matrix1.MatrixDeterminant(), 1e-9);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testGetterAndSetter() {


        UpTriangleMatrix upTriangleMatrix = new UpTriangleMatrix(2);
        upTriangleMatrix.changeElemInd(0, 0, 1);
        upTriangleMatrix.changeElemInd(1, 0, 0);
        upTriangleMatrix.changeElemInd(1, 1, 0);

        assertEquals(0, upTriangleMatrix.getElemInd(0, 1), 1e-9);

        /* Филиппов А.В. 10.06.2020 Комментарий не удалять.
        Не работает! В вашей матрице нет координаты 2, но метод что-то возвращает!
        Поправить все getElemInd и changeElemInd
        */
        upTriangleMatrix.getElemInd(0, 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFailSetterElem() {
        DiagMatrix diagMatrix1 = new DiagMatrix(2);
        diagMatrix1.changeElemInd(0, 0, 1);
        diagMatrix1.changeElemInd(1, 1, 2);
        diagMatrix1.changeElemInd(0, 1, 2);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testFailGetter() {
        DiagMatrix diagMatrix1 = new DiagMatrix(2);
        diagMatrix1.changeElemInd(1, 0, 1);
        diagMatrix1.changeElemInd(1, 1, 2);

        UpTriangleMatrix upTriangleMatrix = new UpTriangleMatrix(2);
        upTriangleMatrix.changeElemInd(0, 0, 0);
        upTriangleMatrix.changeElemInd(1, 0, 0);
        upTriangleMatrix.changeElemInd(1, 1, 0);


        diagMatrix1.getElemInd(-2, 1000);

    }

    @Test
    public void testTriangleMatrix() {
        UpTriangleMatrix upTriangleMatrix = new UpTriangleMatrix(2);
        upTriangleMatrix.changeElemInd(0, 0, 0);
        upTriangleMatrix.changeElemInd(0, 0, 1);
        upTriangleMatrix.changeElemInd(1, 1, 0);
        DiagMatrix diagMatrix = new DiagMatrix(new double[]{1, 2});


        DiagMatrix diagMatrix1 = new DiagMatrix(2);
        diagMatrix1.changeElemInd(1, 0, 0);
        diagMatrix1.changeElemInd(0, 0, 1);
        diagMatrix1.changeElemInd(1, 1, 1);

        assertEquals(2.0, diagMatrix.MatrixDeterminant(), 1e-9);
        assertEquals(1.0, diagMatrix1.MatrixDeterminant(), 1e-9);
        assertEquals(0, upTriangleMatrix.MatrixDeterminant(), 1e-9);

    }

}